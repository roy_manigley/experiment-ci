# Personenverwaltung
> **[Aufgabe: Function Point Schätzung](https://moodle.hftm.ch/mod/page/view.php?id=57609) zum Kurs IN233 Software Engineering**  
>
>Ausgangslage:  
>...ist ein kleiner Prototyp, den ihr mit egal welcher Technologie erstellen sollt:
>- Eine Datenbanktabelle Person mit den Spalten Name, Vorname, Geburtsdatum und Heimatort (Interne Daten, mittel)
>- Eine Datenbanktabelle Adresse mit den Spalten Strasse, Nummer, gültig ab, gültig bis (interne Daten, mittel)
>- Eine Datenbanktabelle Ortschaft mit den Spalten PLZ und Ort (interne Daten, einfach)
>- Die Beziehungen sind wie folgt definiert: Eine Person kann mehrere Adressen haben, nur eine ist jeweils gültig, eine Adresse hat genau eine Ortschaft
>- Es muss in einem grafischen Benutzerinterface (GUI) möglich sein,
>  - neue Personen zu erfassen
>  - bestehende Personen zu löschen
>  - bestehende Personen zu mutieren
>  - einer Person eine neue Adresse hinzuzufügen
>  - Ortschaften in einem Ortschaftsverzeichnis zu pflegen (neu, bearbeiten, löschen)
>  - Eine Liste aller Personen mit allen Adressen am Bildschirm auszugeben
>
>**Aufgabe**
>
>1. Erstellt kurz ein paar Handskizzen, wie diese Applikation aussehen soll
>2. Ermittelt aus den Skizzen die Anzahl Transaktionen (Input, Output, Abfragen)
>3. Versucht den Schwierigkeitsgrad abzuschätzen
>4. Berechnet die Function Points in einem Excel Sheet
>5. Programmiert die Applikation mit einer Technologie nach Wahl (die Cracks sollen ran, die anderen unterstützen). Der Aufwand dafür sollte 5 Stunden nicht überschreiten. Fragt, wenn ihr Fragen habt. Schön ist nicht wichtig. Nur die Funktionalität muss stimmen. Messt den Aufwand so genau wie möglich für die einzelnen Function Points.
>6. Erstellt eine Auswertung, welche FPs wie lange in der Umsetzung brauchten
>7. Gebt die Auswertung und die Applikation inkl. Source Code ab
> 
>Die Aufgabe ist notenrelevant und wird wie eine Prüfung gewertet.
> 
>**Beurteilungskriterien**
>- Vollständigkeit der Umsetzung
>- Vollständigkeit und Nachvollziehbarkeit der Function Point Aufstellung
>- Nachvollziehbarkeit der Aufwandsdokumentation
>- Wiederverwendbarkeit der Kalkulation

#  REST API
## Requirements
- JDK 11
- Maven

## Commands

### Build
`mvn clean package`

### Run (developpment)
`mvn clean spring-boot:run`

### Run (Java 11)
`java -jar target/personen-verwaltung-1.0.jar`



## REST API `http://localhost:8080`

### Ort `/api/ort`
- create  
```
curl -X POST "http://localhost:8080/api/ort/" \
    -H "Content-Type: application/json" \
    --data '{"ort":"Solothurn", "plz": "4500"}'
```
- read  
```
curl -X GET "http://localhost:8080/api/ort/" \
    -H "Content-Type: application/json"
```
```
curl -X GET "http://localhost:8080/api/ort/1" \
    -H "Content-Type: application/json"
```
- update  
```
curl -X PUT "http://localhost:8080/api/ort/1" \
    -H "Content-Type: application/json" \
    --data '{"id":1, "ort":"Zolothurn", "plz": "4500"}'
```
- delete  
```
curl -X DELETE "http://localhost:8080/api/ort/1" \
    -H "Content-Type: application/json"
```

### Person `/api/person`
- create  
```
curl -X POST "http://localhost:8080/api/person/" \
    -H "Content-Type: application/json" \
    --data '{"name":"Lopez", "vorname": "Heidi", "geburtsdatum": "2000-05-20", "heimatort": {"id":1,"ort":"Solothurn","plz":"4500"} }'
```
- read  
```
curl -X GET "http://localhost:8080/api/person/" \
    -H "Content-Type: application/json"
```
```
curl -X GET "http://localhost:8080/api/person/1" \
    -H "Content-Type: application/json"
```
- update  
```
curl -X PUT "http://localhost:8080/api/person/1" \
    -H "Content-Type: application/json" \
    --data '{"id": 1, "name":"Lopez", "vorname": "Heidi", "geburtsdatum": "2000-12-20", "heimatort": {"id":1,"ort":"Solothurn","plz":"4500"} }'
```
- delete  
```
curl -X DELETE "http://localhost:8080/api/person/1" \
    -H "Content-Type: application/json"
```

### Adresse `/api/person/{idPerson}/adressen`
- create  
```
curl -X POST "http://localhost:8080/api/person/1/adressen/" \
    -H "Content-Type: application/json" \
    --data '{ "strasse": "Hauptstrasse", "nummer": "88 B", "ort": { "id": 1, "ort":"Solothurn", "plz": "4500"}, "von": "2019-01-01"}'
```
- read  
```
curl -X GET "http://localhost:8080/api/person/1/adressen/" \
    -H "Content-Type: application/json"
```

```
curl -X GET "http://localhost:8080/api/person/1/adressen/1" \
    -H "Content-Type: application/json"
```
- update
```
curl -X PUT "http://localhost:8080/api/person/1/adressen/1" \
    -H "Content-Type: application/json" \
    --data '{ "id":1, "strasse": "Hauptstrasse", "nummer": "88 B", "ort": { "id": 1, "ort":"Solothurn", "plz": "4500"}, "von": "2019-01-01", "bis": "2019-02-01"}'
```
- delete 
```
curl -X DELETE "http://localhost:8080/api/person/1/adressen/1" \
    -H "Content-Type: application/json"
```
