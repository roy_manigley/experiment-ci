package ch.hftm.se.personenverwaltung.utils;

import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class DateUtil {

    private static final DateTimeFormatter ISO_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static String toIsoDate(TemporalAccessor date) {
        return ISO_DATE_FORMAT.format(date);
    }
}
