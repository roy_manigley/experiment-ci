package ch.hftm.se.personenverwaltung.rest;


import ch.hftm.se.personenverwaltung.model.Ort;
import ch.hftm.se.personenverwaltung.service.OrtService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class OrtControllerPayloadTest {

    private final String ORT = "Biel";
    private final Long ID = 99L;
    private final String PLZ = "2502";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrtService ortService;

    @BeforeEach
    public void init() {
        Ort ort = new Ort();
        ort.setId(ID);
        ort.setOrt(ORT);
        ort.setPlz(PLZ);
        when(ortService.fetchRecordById(anyLong())).thenReturn(Optional.of(ort));
    }

    @Test
    public void testPayloadResponse_byId_success() throws Exception {

        final MvcResult result = mockMvc.perform(get("/api/ort/" + ID.intValue()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID.intValue())))
                .andExpect(jsonPath("$.ort", is(ORT)))
                .andExpect(jsonPath("$.plz", is(PLZ)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        final Logger LOG = LoggerFactory.getLogger(OrtControllerPayloadTest.class);

        LOG.info(result.getResponse().getContentAsString());
        verify(ortService, times(1)).fetchRecordById(anyLong());

    }
}
