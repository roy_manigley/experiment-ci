package ch.hftm.se.personenverwaltung.rest;


import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.model.Ort;
import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.service.PersonService;
import ch.hftm.se.personenverwaltung.utils.DateUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonControllerPayloadTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonService personService;

    private final Person person = new Person();

    @BeforeEach
    public void init() {

        final Ort heimatort = new Ort();
        heimatort.setId(22L);
        heimatort.setOrt("Texas");
        heimatort.setPlz("0815");

        final Ort adresseOrt = new Ort();
        adresseOrt.setId(22L);
        adresseOrt.setOrt("Malibu");
        adresseOrt.setPlz("7750");

        final Adresse adresse = new Adresse();
        adresse.setId(45L);
        adresse.setStrasse("La Strasse");
        adresse.setNummer("2");
        adresse.setOrt(adresseOrt);
        adresse.setVon(LocalDate.now().minus(1, ChronoUnit.MONTHS));
        adresse.setBis(LocalDate.now().plus(1, ChronoUnit.MONTHS));

        person.setId(99L);
        person.setName("Powers");
        person.setVorname("Austin");
        person.setGeburtsdatum(LocalDate.of(1960, 5, 20));
        person.setHeimatort(heimatort);
        person.getAdressen().add(adresse);
        when(personService.fetchRecordById(anyLong())).thenReturn(Optional.of(person));
    }

    @Test
    public void testPayloadResponse_byId_success() throws Exception {

        final Adresse adresseFromPerson = (Adresse) person.getAdressen().toArray()[0];
        final Adresse gueltigeAdresseFromPerson = person.getGueltigeAdresse();
        final MvcResult result = mockMvc.perform(get("/api/person/" + person.getId().intValue()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(person.getId().intValue())))
                .andExpect(jsonPath("$.name", is(person.getName())))
                .andExpect(jsonPath("$.vorname", is(person.getVorname())))
                .andExpect(jsonPath("$.heimatort.id", is(person.getHeimatort().getId().intValue())))
                .andExpect(jsonPath("$.heimatort.ort", is(person.getHeimatort().getOrt())))
                .andExpect(jsonPath("$.heimatort.plz", is(person.getHeimatort().getPlz())))
                .andExpect(jsonPath("$.gueltigeAdresse.id", is(gueltigeAdresseFromPerson.getId().intValue())))
                .andExpect(jsonPath("$.gueltigeAdresse.strasse", is(gueltigeAdresseFromPerson.getStrasse())))
                .andExpect(jsonPath("$.gueltigeAdresse.nummer", is(gueltigeAdresseFromPerson.getNummer())))
                .andExpect(jsonPath("$.gueltigeAdresse.ort.id", is(gueltigeAdresseFromPerson.getOrt().getId().intValue())))
                .andExpect(jsonPath("$.gueltigeAdresse.ort.ort", is(gueltigeAdresseFromPerson.getOrt().getOrt())))
                .andExpect(jsonPath("$.gueltigeAdresse.ort.plz", is(gueltigeAdresseFromPerson.getOrt().getPlz())))
                .andExpect(jsonPath("$.gueltigeAdresse.von", is(DateUtil.toIsoDate(gueltigeAdresseFromPerson.getVon()))))
                .andExpect(jsonPath("$.gueltigeAdresse.bis", is(DateUtil.toIsoDate(gueltigeAdresseFromPerson.getBis()))))
                .andExpect(jsonPath("$.adressen[0].id", is(adresseFromPerson.getId().intValue())))
                .andExpect(jsonPath("$.adressen[0].strasse", is(adresseFromPerson.getStrasse())))
                .andExpect(jsonPath("$.adressen[0].nummer", is(adresseFromPerson.getNummer())))
                .andExpect(jsonPath("$.adressen[0].ort.id", is(adresseFromPerson.getOrt().getId().intValue())))
                .andExpect(jsonPath("$.adressen[0].ort.ort", is(adresseFromPerson.getOrt().getOrt())))
                .andExpect(jsonPath("$.adressen[0].ort.plz", is(adresseFromPerson.getOrt().getPlz())))
                .andExpect(jsonPath("$.adressen[0].von", is(DateUtil.toIsoDate(adresseFromPerson.getVon()))))
                .andExpect(jsonPath("$.adressen[0].bis", is(DateUtil.toIsoDate(adresseFromPerson.getBis()))))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        final Logger LOG = LoggerFactory.getLogger(PersonControllerPayloadTest.class);

        LOG.info(result.getResponse().getContentAsString());
        verify(personService, times(1)).fetchRecordById(anyLong());

    }
}
