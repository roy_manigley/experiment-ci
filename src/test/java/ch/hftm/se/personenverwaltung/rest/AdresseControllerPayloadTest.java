package ch.hftm.se.personenverwaltung.rest;


import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.model.Ort;
import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.service.AdresseService;
import ch.hftm.se.personenverwaltung.service.PersonService;
import ch.hftm.se.personenverwaltung.utils.DateUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class AdresseControllerPayloadTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AdresseService adresseService;

    private final Adresse adresse = new Adresse();
    @BeforeEach
    public void init() {

        final Ort ort = new Ort();
        ort.setId(22L);
        ort.setOrt("Biel");
        ort.setPlz("2502");

        final Person person = new Person();
        person.setId(789L);
        person.setVorname("Mario");

        adresse.setId(45L);
        adresse.setStrasse("Dufourstrasse");
        adresse.setNummer("45");
        adresse.setOrt(ort);
        adresse.setVon(LocalDate.now().minus(1, ChronoUnit.DAYS));
        adresse.setPerson(person);
        adresse.setBis(null);

        when(adresseService.fetchRecordById(anyLong())).thenReturn(Optional.of(adresse));
    }

    @Test
    public void testPayloadResponse_byIdWithValidIdPerson_success() throws Exception {

        final MvcResult result = mockMvc.perform(get("/api/person/"+ adresse.getPerson().getId() +"/adressen/" + adresse.getId().intValue()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(adresse.getId().intValue())))
                .andExpect(jsonPath("$.strasse", is(adresse.getStrasse())))
                .andExpect(jsonPath("$.nummer", is(adresse.getNummer())))
                .andExpect(jsonPath("$.ort.id", is(adresse.getOrt().getId().intValue())))
                .andExpect(jsonPath("$.ort.ort", is(adresse.getOrt().getOrt())))
                .andExpect(jsonPath("$.ort.plz", is(adresse.getOrt().getPlz())))
                .andExpect(jsonPath("$.von", is(DateUtil.toIsoDate(adresse.getVon()))))
                .andExpect(jsonPath("$.bis", is(nullValue())))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        final Logger LOG = LoggerFactory.getLogger(AdresseControllerPayloadTest.class);

        LOG.info(result.getResponse().getContentAsString());
        verify(adresseService, times(1)).fetchRecordById(anyLong());
    }

    @Test
    public void testPayloadResponse_byIdWithInvalidIdPerson_success() throws Exception {

        final MvcResult result = mockMvc.perform(get("/api/person/"+ (adresse.getPerson().getId() + 10) +"/adressen/" + adresse.getId().intValue()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        final Logger LOG = LoggerFactory.getLogger(AdresseControllerPayloadTest.class);

        LOG.info(result.getResponse().getContentAsString());
        verify(adresseService, times(1)).fetchRecordById(anyLong());
    }
}
