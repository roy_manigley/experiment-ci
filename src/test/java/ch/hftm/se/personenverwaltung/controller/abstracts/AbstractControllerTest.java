package ch.hftm.se.personenverwaltung.controller.abstracts;

import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.model.abstracts.Keyable;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractCrudService;
import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public abstract class AbstractControllerTest<T extends Keyable> {

    private final Class<T> clazz;

    public AbstractControllerTest(Class<T> clazz) {
        this.clazz = clazz;
    }


    @Test
    public void testFetchAllRecords_listOf3_success() {

        getController().fetchAllRecords();
        verify(getService(), times(1)).fetchAllRecords();
    }


    @Test
    public void testFetchRecordById_success() {

        getController().fetchRecordById(59L);
        verify(getService(), times(1)).fetchRecordById(anyLong());
    }

    @Test
    public void testCreateRecord_success() {

        getController().createRecord(any());
        verify(getService(), times(1)).createRecord(any());
    }

    @Test
    public void testUpdateRecord_success() {

        final T keyable = mock(clazz);
        getController().updateRecord(anyLong(), keyable);
        verify(getService(), times(1)).updateRecord(any());
    }

    @Test
    public void testDeleteRecord_success() {

        getController().deleteRecord(anyLong());
        verify(getService(), times(1)).deleteRecordById(anyLong());
    }

    public abstract GenericCrudController<T> getController();
    public abstract AbstractCrudService<T> getService();
}
