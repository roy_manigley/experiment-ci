package ch.hftm.se.personenverwaltung.controller.integration;

import ch.hftm.se.personenverwaltung.controller.AdresseController;
import ch.hftm.se.personenverwaltung.controller.PersonController;
import ch.hftm.se.personenverwaltung.controller.integration.abstracts.AbstractControllerDbUnitIT;
import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.model.Ort;
import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.repository.OrtRepository;
import ch.hftm.se.personenverwaltung.repository.PersonRepository;
import ch.hftm.se.personenverwaltung.service.AdresseService;
import ch.hftm.se.personenverwaltung.service.PersonService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AdresseControllerIT extends AbstractControllerDbUnitIT {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private OrtRepository ortRepository;

    @Autowired
    private AdresseController adresseController;


    @Test
    public void findTests() {


        //fetchRecordById_NotExistingPersonId_noException
        assertDoesNotThrow(() -> adresseController.fetchRecordById(999L, 999L));

        // fetchRecordById_ExistingPersonIdAndNotExistingAdresseId_noException

        assertDoesNotThrow(() -> adresseController.fetchRecordById(1L, 999L));

        // fetchRecordById_ExistingPersonIdAndAdresseId_personFound

        final Optional<Adresse> adresse = adresseController.fetchRecordById(1L, 1L);
        assertThat(adresse.isPresent(), is(true));
        assertThat(adresse.get().getId(), is(1L));
        assertThat(adresse.get().getOrt().getId(), is(3L));
        assertThat(adresse.get().getPerson().getId(), is(1L));
    }

    @Test
    public void fetchAllRecords_2AdressenForPersonIdOneFromDBUnitFile_correctOrder() {

        final List<Adresse> adressenPerson_01 = adresseController.fetchAllRecords(1L);

        assertThat(adressenPerson_01, not(empty()));
        assertThat(adressenPerson_01.size(), is(2));

        assertThat(adressenPerson_01.get(0).getId(), is(1L));
        assertThat(adressenPerson_01.get(0).getBis(), nullValue());

        assertThat(adressenPerson_01.get(1).getId(), is(2L));
        assertThat(adressenPerson_01.get(1).getBis(), notNullValue());

    }

    @Test
    public void createRecord_AdresseWithPersonAndOrt_adresseSaved() {

        final Person person = new Person();
        person.setName("La Bamba");
        person.setVorname("Rambo");

        final Ort ort = new Ort();
        ort.setPlz("0815");
        ort.setOrt("Ortien");

        Person savedPerson = personRepository.saveAndFlush(person);
        Ort savedOrt = ortRepository.saveAndFlush(ort);

        final Adresse adresse = new Adresse();
        adresse.setPerson(savedPerson);
        adresse.setOrt(savedOrt);
        adresse.setStrasse("Mittelstrasse");
        adresse.setNummer("789");
        adresse.setVon(LocalDate.now());

        final Adresse savedAdresse = adresseController.createRecord(savedPerson.getId(), adresse);

        assertThat(savedAdresse.getId(), notNullValue());
        assertThat(savedAdresse.getPersonId(), is(savedPerson.getId()));
        assertThat(savedAdresse.getPerson(), is(savedPerson));
        assertThat(savedAdresse.getOrt(), is(savedOrt));
        assertThat(savedAdresse.getStrasse(), is(adresse.getStrasse()));
        assertThat(savedAdresse.getNummer(), is(adresse.getNummer()));
        assertThat(savedAdresse.getVon(), is(adresse.getVon()));
        assertThat(savedAdresse.getBis(), nullValue());
    }


    @Test
    public void createRecord_AdresseWithId_adresseNotSaved() {

        final Person person = new Person();
        person.setName("La Bamba");
        person.setVorname("Rambo");

        final Ort ort = new Ort();
        ort.setPlz("0815");
        ort.setOrt("Ortien");

        Person savedPerson = personRepository.saveAndFlush(person);
        Ort savedOrt = ortRepository.saveAndFlush(ort);

        final Adresse adresse = new Adresse();
        adresse.setId(99L);
        adresse.setPerson(savedPerson);
        adresse.setOrt(savedOrt);
        adresse.setStrasse("Mittelstrasse");
        adresse.setNummer("789");
        adresse.setVon(LocalDate.now());

        assertThrows(IllegalArgumentException.class, () -> adresseController.createRecord(savedPerson.getId(), adresse));

        final Optional<Adresse> fetchedRecordById = adresseController.fetchRecordById(savedPerson.getId(), 99L);
        assertThat(fetchedRecordById.isEmpty(), is(true));
    }

    @Test
    public void createRecord_AdresseWithInvalidIdPerson_adresseSaved() {

        final long INVALID_ID_PERSON = 9999L;

        final Person person = new Person();
        person.setName("La Bamba");
        person.setVorname("Rambo");

        final Ort ort = new Ort();
        ort.setPlz("0815");
        ort.setOrt("Ortien");

        Person savedPerson = personRepository.saveAndFlush(person);
        Ort savedOrt = ortRepository.saveAndFlush(ort);

        final Adresse adresse = new Adresse();
        adresse.setPerson(savedPerson);
        adresse.setOrt(savedOrt);
        adresse.setStrasse("Mittelstrasse");
        adresse.setNummer("789");
        adresse.setVon(LocalDate.now());

        assertThrows(IllegalArgumentException.class, () -> adresseController.createRecord(INVALID_ID_PERSON, adresse));

        final Optional<Adresse> fetchedRecordById = adresseController.fetchRecordById(savedPerson.getId(), INVALID_ID_PERSON);
        assertThat(fetchedRecordById.isEmpty(), is(true));
    }

    @Test
    public void updateRecord_AdresseChangeStrasse_adresseUpdated() {

        final String RANDOM_STRASSE = new BigInteger(128, new SecureRandom()).toString(32);

        Adresse adresse = adresseController.fetchAllRecords(1L).get(0);
        adresse.setStrasse(RANDOM_STRASSE);

        adresseController.updateRecord(1L, adresse.getId(), adresse);

        final Optional<Adresse> fetchedRecordById = adresseController.fetchRecordById(1L, adresse.getId());

        assertThat(fetchedRecordById.isPresent(), is(true));
        assertThat(fetchedRecordById.get().getStrasse(), is(RANDOM_STRASSE));
    }


    @Test
    public void updateRecord_AdresseWithNoId_adresseNotUpdated() {
        final String RANDOM_STRASSE = new BigInteger(128, new SecureRandom()).toString(32);

        Adresse adresse = adresseController.fetchAllRecords(1L).get(0);
        final Long adresseId = adresse.getId();
        adresse.setStrasse(RANDOM_STRASSE);

        assertThrows(IllegalArgumentException.class, () -> adresseController.updateRecord(1L, null, adresse));

        final Optional<Adresse> fetchedRecordById = adresseController.fetchRecordById(1L, adresseId);
        assertThat(fetchedRecordById.isPresent(), is(true));
        assertThat(fetchedRecordById.get().getStrasse(), is(not(RANDOM_STRASSE)));
    }

    @Test
    public void updateRecord_AdresseWithInvalidPersonId_adresseNotUpdated() {
        final String RANDOM_STRASSE = new BigInteger(128, new SecureRandom()).toString(32);

        Adresse adresse = adresseController.fetchAllRecords(1L).get(0);
        adresse.setStrasse(RANDOM_STRASSE);

        final long INVALID_ID_PERSON = 9999L;
        assertThrows(IllegalArgumentException.class, () -> adresseController.updateRecord(INVALID_ID_PERSON, adresse.getId(), adresse));

        final Optional<Adresse> fetchedRecordById = adresseController.fetchRecordById(adresse.getPersonId(), adresse.getId());
        assertThat(fetchedRecordById.isPresent(), is(true));
        assertThat(fetchedRecordById.get().getStrasse(), is(not(RANDOM_STRASSE)));
        assertThat(fetchedRecordById.get().getPersonId(), is(not(INVALID_ID_PERSON)));
    }

    @Test
    public void deleteTests() {


        // deleteRecord_ExistingAdressIdWithRelatedPerson_adresseDeletedPersonStays

        Adresse adresse = adresseController.fetchAllRecords(1L).get(0);

        assertDoesNotThrow(() -> adresseController.deleteRecord(adresse.getPersonId(), adresse.getId()));

        final Optional<Adresse> fetchedRecordById = adresseController.fetchRecordById(adresse.getPersonId(), adresse.getId());
        assertThat(fetchedRecordById.isEmpty(), is(true));

        final Optional<Person> relatedPerson = personRepository.findById(adresse.getPersonId());
        assertThat(relatedPerson, notNullValue());

        // deleteRecord_NotExistingAdresseIdValidPersonId_exceptionThrown

        assertThrows(IllegalArgumentException.class, () -> adresseController.deleteRecord(1L, 999L));

        // deleteRecord_ExistingInvalidPersonId_exceptionThrown

        assertThrows(IllegalArgumentException.class, () -> adresseController.deleteRecord(999L, 1L));
    }
}
