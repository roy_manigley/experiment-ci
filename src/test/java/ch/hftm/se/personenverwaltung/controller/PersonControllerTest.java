package ch.hftm.se.personenverwaltung.controller;

import ch.hftm.se.personenverwaltung.controller.abstracts.AbstractControllerTest;
import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.service.PersonService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PersonControllerTest extends AbstractControllerTest<Person> {

    @InjectMocks
    private PersonController personController;

    @Mock
    private PersonService personService;

    public PersonControllerTest() {
        super(Person.class);
    }

    @Override
    public PersonController getController() {
        return personController;
    }

    @Override
    public PersonService getService() {
        return personService;
    }
}