package ch.hftm.se.personenverwaltung.controller;

import ch.hftm.se.personenverwaltung.controller.abstracts.AbstractControllerTest;
import ch.hftm.se.personenverwaltung.model.Ort;
import ch.hftm.se.personenverwaltung.service.OrtService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class OrtControllerTest extends AbstractControllerTest<Ort> {

    @InjectMocks
    private OrtController ortController;

    @Mock
    private OrtService ortService;

    public OrtControllerTest() {
        super(Ort.class);
    }

    @Override
    public OrtController getController() {
        return ortController;
    }

    @Override
    public OrtService getService() {
        return ortService;
    }
}