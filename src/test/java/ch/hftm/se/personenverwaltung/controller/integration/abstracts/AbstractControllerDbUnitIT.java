package ch.hftm.se.personenverwaltung.controller.integration.abstracts;

import ch.hftm.se.personenverwaltung.controller.integration.PersonControllerIT;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.SQLException;

@SpringBootTest
public class AbstractControllerDbUnitIT<T> {

    @Autowired
    private DataSource dataSource;

    @BeforeEach
    public void before() {

        final String DB_UNIT_FILE = "dbunit/transformed-data.xml";

        try {

            IDatabaseConnection connection = new DatabaseConnection(dataSource.getConnection());
            connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new HsqldbDataTypeFactory());

            FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
            flatXmlDataSetBuilder.setColumnSensing(true);
            InputStream dataSet = PersonControllerIT.class.getClassLoader().getResourceAsStream(DB_UNIT_FILE);
            IDataSet dataset = flatXmlDataSetBuilder.build(dataSet);
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);


        } catch (DatabaseUnitException | SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void after() {
        try {
            dataSource.getConnection().close();
        } catch (SQLException e) {
            Assertions.fail(e);
        }
    }
}
