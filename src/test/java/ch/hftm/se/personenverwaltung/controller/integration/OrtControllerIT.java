package ch.hftm.se.personenverwaltung.controller.integration;

import ch.hftm.se.personenverwaltung.controller.OrtController;
import ch.hftm.se.personenverwaltung.controller.PersonController;
import ch.hftm.se.personenverwaltung.controller.integration.abstracts.AbstractControllerDbUnitIT;
import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.model.Ort;
import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.repository.PersonRepository;
import ch.hftm.se.personenverwaltung.service.AdresseService;
import org.aspectj.weaver.ast.Or;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class OrtControllerIT extends AbstractControllerDbUnitIT {


    @Autowired
    private EntityManager entityManager;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private OrtController ortController;

    @Test
    public void fetchRecordById_NotExistingOrtId_noException() {

        assertDoesNotThrow(() -> ortController.fetchRecordById(999L));
    }

    @Test
    public void fetchRecordById_ExistingOrtId_personFound() {

        final Optional<Ort> ort = ortController.fetchRecordById(1L);
        assertThat(ort.isPresent(), is(true));
        assertThat(ort.get().getOrt(), is("Biel"));
        assertThat(ort.get().getPlz(), is("2500"));
    }

    @Test
    public void fetchAllRecords_3OrtsFromDBUnitFile_correctOrder() {

        final List<Ort> ort = ortController.fetchAllRecords();
        assertThat(ort, not(empty()));
        assertThat(ort.get(0).getOrt(), is("Bern"));
        assertThat(ort.get(1).getOrt(), is("Biel"));
        assertThat(ort.get(2).getOrt(), is("Solothurn"));
    }

    @Test
    public void createRecord_OrtWithOrtAndPlz_ortSaved() {

        final Ort ort = new Ort();
        ort.setOrt("Zollikofen");
        ort.setPlz("3052");

        Ort savedOt = ortController.createRecord(ort);
        assertThat(savedOt.getId(), notNullValue());
        assertThat(savedOt.getOrt(), is(ort.getOrt()));
        assertThat(savedOt.getPlz(), is(ort.getPlz()));
    }

    @Test
    public void createRecord_OrtWithId_ortNotSaved() {

        final Ort ort = new Ort();
        ort.setId(99L);
        ort.setOrt("Zollikofen");
        ort.setPlz("3052");

        assertThrows(IllegalArgumentException.class, () -> ortController.createRecord(ort));

        final Optional<Ort> fetchedRecordById = ortController.fetchRecordById(99L);
        assertThat(fetchedRecordById.isEmpty(), is(true));
    }

    @Test
    public void updateRecord_OrtChangePlz_ortUpdated() {

        final String RANDOM_PLZ = new BigInteger(128, new SecureRandom()).toString(32);

        Ort ort = ortController.fetchAllRecords().get(0);
        ort.setPlz(RANDOM_PLZ);

        ortController.updateRecord(ort.getId(), ort);

        final Optional<Ort> fetchedRecordById = ortController.fetchRecordById(ort.getId());

        assertThat(fetchedRecordById.isPresent(), is(true));
        assertThat(fetchedRecordById.get().getPlz(), is(RANDOM_PLZ));
    }

    @Test
    public void updateRecord_OrtWithNoId_ortNotUpdated() {

        final String RANDOM_PLZ = new BigInteger(128, new SecureRandom()).toString(32);

        Ort ort = ortController.fetchAllRecords().get(0);
        final Long ortId = ort.getId();
        ort.setPlz(RANDOM_PLZ);

        assertThrows(IllegalArgumentException.class, () -> ortController.updateRecord(null, ort));

        final Optional<Ort> fetchedRecordById = ortController.fetchRecordById(ortId);
        assertThat(fetchedRecordById.isPresent(), is(true));
        assertThat(fetchedRecordById.get().getPlz(), is(not(RANDOM_PLZ)));
    }


    @Test
    public void deleteRecord_Tests() {


        // deleteRecord_ExistingOrtIdRelatedToPerson_ortNotDeleted() {

        Person person = personRepository.findAll().get(0);

        assertThat(person.getHeimatort(), notNullValue());

        final Long ORT_ID = person.getHeimatort().getId();

        assertThrows(RuntimeException.class, () -> ortController.deleteRecord(ORT_ID));

        final Optional<Ort> fetchedRecordById = ortController.fetchRecordById(ORT_ID);
        assertThat(fetchedRecordById.isPresent(), is(true));

        // deleteRecord_ExistingOrtIdNoRelatedToPersonOrAdresse_ortNotDeleted() {

        final Ort ort = new Ort();
        ort.setOrt("Zollikofen");
        ort.setPlz("3052");

        Ort savedOt = ortController.createRecord(ort);
        assertThat(savedOt.getId(), notNullValue());

        ortController.deleteRecord(savedOt.getId());

        final Optional<Ort> recordByIdAfterDelete = ortController.fetchRecordById(savedOt.getId());
        assertThat(recordByIdAfterDelete.isEmpty(), is(true));

        // deleteRecord_NotExistingOrtId_exceptionThrown() {

        assertThrows(IllegalArgumentException.class, () -> ortController.deleteRecord(999L));
    }
}
