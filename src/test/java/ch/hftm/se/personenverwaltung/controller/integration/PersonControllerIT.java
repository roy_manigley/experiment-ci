package ch.hftm.se.personenverwaltung.controller.integration;

import ch.hftm.se.personenverwaltung.controller.PersonController;
import ch.hftm.se.personenverwaltung.controller.integration.abstracts.AbstractControllerDbUnitIT;
import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.repository.AdresseRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class PersonControllerIT extends AbstractControllerDbUnitIT {


    @Autowired
    private EntityManager entityManager;

    @Autowired
    private AdresseRepository adresseRepository;

    @Autowired
    private PersonController personController;

    @Test
    public void fetchRecordById_NotExistingPersonId_noException() {

        assertDoesNotThrow(() -> personController.fetchRecordById(999L));
    }

    @Test
    public void fetchRecordById_ExistingPersonId_personFound() {

        final Optional<Person> person = personController.fetchRecordById(1L);
        assertThat(person.isPresent(), is(true));
        assertThat(person.get().getName(), is("Lopez"));
        assertThat(person.get().getVorname(), is("Heidi"));
    }

    @Test
    public void fetchAllRecords_3PersonsFromDBUnitFile_correctOrder() {

        final List<Person> people = personController.fetchAllRecords();
        assertThat(people, not(empty()));
        assertThat(people.get(0).getName(), is("Lebovski"));
        assertThat(people.get(1).getName(), is("Lopez"));
        assertThat(people.get(2).getName(), is("No Valid Address"));
    }

    @Test
    public void createRecord_PersonWithVorAndNachname_personSaved() {

        final Person person = new Person();
        person.setName("La Bamba");
        person.setVorname("Rambo");

        Person savedPerson = personController.createRecord(person);
        assertThat(savedPerson.getId(), notNullValue());
        assertThat(savedPerson.getName(), is(person.getName()));
        assertThat(savedPerson.getVorname(), is(person.getVorname()));
    }

    @Test
    public void createRecord_PersonWithId_personNotSaved() {

        final Person person = new Person();
        person.setId(99L);
        person.setName("La Bamba");
        person.setVorname("Rambo");

        assertThrows(IllegalArgumentException.class, () -> personController.createRecord(person));

        final Optional<Person> fetchedRecordById = personController.fetchRecordById(99L);
        assertThat(fetchedRecordById.isEmpty(), is(true));
    }

    @Test
    public void updateRecord_PersonChangeNachname_personUpdated() {

        final String RANDOM_NAME = new BigInteger(128, new SecureRandom()).toString(32);

        Person person = personController.fetchAllRecords().get(0);
        person.setName(RANDOM_NAME);

        personController.updateRecord(person.getId(), person);

        final Optional<Person> fetchedRecordById = personController.fetchRecordById(person.getId());

        assertThat(fetchedRecordById.isPresent(), is(true));
        assertThat(fetchedRecordById.get().getName(), is(RANDOM_NAME));
    }

    @Test
    public void updateRecord_PersonWithNoId_personNotUpdated() {
        final String RANDOM_NAME = new BigInteger(128, new SecureRandom()).toString(32);

        Person person = personController.fetchAllRecords().get(0);
        final Long personId = person.getId();
        person.setName(RANDOM_NAME);

        assertThrows(IllegalArgumentException.class, () -> personController.updateRecord(null, person));

        final Optional<Person> fetchedRecordById = personController.fetchRecordById(personId);
        assertThat(fetchedRecordById.isPresent(), is(true));
        assertThat(fetchedRecordById.get().getName(), is(not(RANDOM_NAME)));
    }


    @Test
    public void deleteRecord_ExistingPersonId_personAndRelatedAdressesDeleted() {

        Person person = personController.fetchAllRecords().get(0);

        assertThat(person.getAdressen(), not(empty()));

        final List<Long> relatedIdAdressen = person.getAdressen().stream()
                .map(a -> a.getId())
                .collect(Collectors.toList());

        assertDoesNotThrow(() -> personController.deleteRecord(person.getId()));

        final Optional<Person> fetchedRecordById = personController.fetchRecordById(person.getId());
        assertThat(fetchedRecordById.isEmpty(), is(true));


        final List<Adresse> adressenByIdPerson = adresseRepository.findAllByIdPerson(person.getId());
        assertThat(adressenByIdPerson, empty());

        final List<Adresse> adressen = entityManager
                .createQuery("FROM Adresse a WHERE a.id IN (:relatedIdAdressen)", Adresse.class)
                .setParameter("relatedIdAdressen", relatedIdAdressen)
                .getResultList();

        assertThat(adressen, empty());
    }

    @Test
    public void deleteRecord_NotExistingPersonId_exceptionThrown() {

        assertThrows(IllegalArgumentException.class, () -> personController.deleteRecord(999L));
    }
}
