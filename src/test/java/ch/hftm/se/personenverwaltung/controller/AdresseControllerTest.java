package ch.hftm.se.personenverwaltung.controller;

import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.model.abstracts.Keyable;
import ch.hftm.se.personenverwaltung.service.AdresseService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AdresseControllerTest {


    @InjectMocks
    private AdresseController adresseController;

    @Mock
    private AdresseService adresseService;


    @Test
    public void testFetchAllRecords_listOf3_success() {

        adresseController.fetchAllRecords(anyLong());
        verify(adresseService, times(1)).findAllByPersonId(anyLong());
    }


    @Test
    public void testFetchRecordByIdAndValidPersonId_success() {

        final Adresse adresse = new Adresse();
        final Person person = mock(Person.class);
        when(person.getId()).thenReturn(99L);
        adresse.setPerson(person);

        when(adresseService.fetchRecordById(anyLong())).thenReturn(Optional.of(adresse));

        adresseController.fetchRecordById(99L, anyLong());
        verify(adresseService, times(1)).fetchRecordById(anyLong());
    }

    @Test
    public void testFetchRecordByIdAndInvalidPersonId_null() {

        final Adresse adresse = new Adresse();
        final Person person = mock(Person.class);
        when(person.getId()).thenReturn(77L);
        adresse.setPerson(person);

        when(adresseService.fetchRecordById(anyLong())).thenReturn(Optional.of(adresse));

        adresseController.fetchRecordById(99L, anyLong());
        verify(adresseService, times(1)).fetchRecordById(anyLong());
    }

    @Test
    public void testCreateRecord_success() {

        adresseController.createRecord(anyLong(), any());
        verify(adresseService, times(1)).createRecord(anyLong(), any());
    }

    @Test
    public void testUpdateRecord_success() {
        final Adresse keyable = new Adresse();
        adresseController.updateRecord(anyLong(), anyLong(), keyable);
        verify(adresseService, times(1)).updateRecord(anyLong(), any());
    }

    @Test
    public void testDeleteRecord_success() {

        adresseController.deleteRecord(anyLong(), anyLong());
        verify(adresseService, times(1)).deleteRecordById(anyLong(), anyLong());
    }
}