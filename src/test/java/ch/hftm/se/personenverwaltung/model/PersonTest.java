package ch.hftm.se.personenverwaltung.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PersonTest {


    private static final LocalDate today = LocalDate.now();
    private static final LocalDate tomorrow = today.plus(1, ChronoUnit.DAYS);
    private static final LocalDate yesterday = today.minus(1, ChronoUnit.DAYS);
    private static final LocalDate lastMonth = today.minus(1, ChronoUnit.MONTHS);
    private static final LocalDate nextMonth = today.plus(1, ChronoUnit.MONTHS);
    private static final LocalDate lastYear = today.minus(1, ChronoUnit.YEARS);
    private static final LocalDate nextYear = today.plus(1, ChronoUnit.YEARS);

    @Test
    public void testGueltigeAdresse_oneGueltigSinceYesterdayOtherUntillYesterday_gueltig() {

        final Adresse gueltigeAdresse = new Adresse();
        final Adresse unGueltigeAdresse = new Adresse();

        gueltigeAdresse.setVon(yesterday);
        gueltigeAdresse.setBis(null);

        unGueltigeAdresse.setVon(lastMonth);
        unGueltigeAdresse.setBis(yesterday);

        final Person person = new Person();
        final List<Adresse> adressen = Arrays.asList(gueltigeAdresse, unGueltigeAdresse);
        person.getAdressen().addAll(adressen);

        final Adresse gueltigeAdresseFromPerson = person.getGueltigeAdresse();
        assertThat(gueltigeAdresseFromPerson, is(gueltigeAdresse));
    }

    @Test
    public void testGueltigeAdresse_oneGueltigSinceYesterdayUntilTomorrowOtherSinceTomorrow_gueltig() {

        final Adresse gueltigeAdresse = new Adresse();
        final Adresse unGueltigeAdresse = new Adresse();

        gueltigeAdresse.setVon(yesterday);
        gueltigeAdresse.setBis(tomorrow);

        unGueltigeAdresse.setVon(tomorrow);
        unGueltigeAdresse.setBis(null);

        final Person person = new Person();
        final List<Adresse> adressen = Arrays.asList(gueltigeAdresse, unGueltigeAdresse);
        person.getAdressen().addAll(adressen);

        final Adresse gueltigeAdresseFromPerson = person.getGueltigeAdresse();
        assertThat(gueltigeAdresseFromPerson, is(gueltigeAdresse));
    }

    @Test
    public void testGueltigeAdresse_oneGueltigFromYesterdayUntillTomorrowOtherNotSinceYesterday_gueltig() {

        final Adresse gueltigeAdresse = new Adresse();
        final Adresse unGueltigeAdresse = new Adresse();

        gueltigeAdresse.setVon(yesterday);
        gueltigeAdresse.setBis(tomorrow);

        unGueltigeAdresse.setVon(lastMonth);
        unGueltigeAdresse.setBis(yesterday);

        final Person person = mock(Person.class);
        final List<Adresse> adressen = Arrays.asList(gueltigeAdresse, unGueltigeAdresse);
        when(person.getAdressen()).thenReturn(new HashSet<>(adressen));
        when(person.getGueltigeAdresse()).thenCallRealMethod();

        assertThat(person.getGueltigeAdresse(), is(gueltigeAdresse));
    }

    @Test
    public void testGueltigeAdresse_oneGueltigFromTomorrowOtherUntillToday_null() {

        final Adresse unGueltigeAdresse_01 = new Adresse();
        final Adresse unGueltigeAdresse_02 = new Adresse();

        unGueltigeAdresse_01.setVon(tomorrow);
        unGueltigeAdresse_01.setBis(null);

        unGueltigeAdresse_02.setVon(lastMonth);
        unGueltigeAdresse_02.setBis(today);

        final Person person = mock(Person.class);
        final List<Adresse> adressen = Arrays.asList(unGueltigeAdresse_01, unGueltigeAdresse_02);
        when(person.getAdressen()).thenReturn(new HashSet<>(adressen));
        when(person.getGueltigeAdresse()).thenCallRealMethod();

        assertThat(person.getGueltigeAdresse(), is(nullValue()));
    }

    @Test
    public void testGueltigeAdresse_oneGueltigFromToayUntillTodayOtherFromTomorrow_null() {

        final Adresse unGueltigeAdresse_01 = new Adresse();
        final Adresse unGueltigeAdresse_02 = new Adresse();

        unGueltigeAdresse_01.setVon(today);
        unGueltigeAdresse_01.setBis(today);

        unGueltigeAdresse_02.setVon(tomorrow);
        unGueltigeAdresse_02.setBis(null);

        final Person person = mock(Person.class);
        final List<Adresse> adressen = Arrays.asList(unGueltigeAdresse_01, unGueltigeAdresse_02);
        when(person.getAdressen()).thenReturn(new HashSet<>(adressen));
        when(person.getGueltigeAdresse()).thenCallRealMethod();

        assertThat(person.getGueltigeAdresse(), is(nullValue()));
    }

    @Test
    public void testGueltigeAdresse_gueltigSinceLastMonthUntilNextMonth_gueltig() {

        final Adresse gueltigeAdresse = new Adresse();

        gueltigeAdresse.setVon(lastMonth);
        gueltigeAdresse.setBis(nextMonth);
        final Person person = new Person();
        person.getAdressen().add(gueltigeAdresse);

        final Adresse gueltigeAdresseFromPerson = person.getGueltigeAdresse();
        assertThat(gueltigeAdresseFromPerson, is(gueltigeAdresse));
    }

    @Test
    public void testGueltigeAdresse_gueltigSinceLastYearUntilNextYear_gueltig() {

        final Adresse gueltigeAdresse = new Adresse();

        gueltigeAdresse.setVon(lastYear);
        gueltigeAdresse.setBis(nextYear);
        final Person person = new Person();
        person.getAdressen().add(gueltigeAdresse);

        final Adresse gueltigeAdresseFromPerson = person.getGueltigeAdresse();
        assertThat(gueltigeAdresseFromPerson, is(gueltigeAdresse));
    }
}