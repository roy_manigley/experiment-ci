package ch.hftm.se.personenverwaltung.service;

import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.repository.AdresseRepository;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractServiceTest;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractCrudService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AdresseServiceTest extends AbstractServiceTest<Adresse> {

    @InjectMocks
    private AdresseService adresseService;

    @Mock
    private AdresseRepository adresseRepository;

    @Mock
    private PersonService personService;

    public AdresseServiceTest() {
        super(Adresse.class);
    }

    @Test
    public void testFindAdress_byPerson_sucess() {

        final Person person = mock(Person.class);
        when(personService.fetchRecordById(any())).thenReturn(Optional.of(person));

        adresseService.findAllByPersonId(1L);

        verify(adresseRepository, times(1)).findAllByIdPerson(anyLong());
    }

    @Test
    public void testFindAdress_byPerson_fail() {

        when(personService.fetchRecordById(any())).thenReturn(Optional.ofNullable(null));

        Assertions.assertThrows(IllegalArgumentException.class, () -> adresseService.findAllByPersonId(1L));

        verify(adresseRepository, times(0)).findAllByIdPerson(anyLong());
    }

    @Test
    public void testCreateAdress_byValidPersonAndValidAdress_sucess() {

        final Adresse adresse = mock(Adresse.class);
        when(adresse.getId()).thenReturn(null);
        final Person person = mock(Person.class);
        when(personService.fetchRecordById(any())).thenReturn(Optional.of(person));

        adresseService.createRecord(1L, adresse);

        verify(adresseRepository, times(1)).save(any());
    }

    @Test
    public void testCreateAdress_byValidPersonAndInvalidAdress_fail() {

        final Adresse adresse = mock(Adresse.class);
        when(adresse.getId()).thenReturn(1L);
        final Person person = mock(Person.class);
        when(personService.fetchRecordById(any())).thenReturn(Optional.of(person));

        Assertions.assertThrows(IllegalArgumentException.class, () -> adresseService.createRecord(1L, adresse));
        verify(adresseRepository, times(0)).save(any());
    }

    @Test
    public void testCreateAdress_byInvalidPersonAndValidAdress_fail() {

        final Adresse adresse = mock(Adresse.class);
        when(personService.fetchRecordById(any())).thenReturn(Optional.ofNullable(null));

        Assertions.assertThrows(IllegalArgumentException.class, () -> adresseService.createRecord(1L, adresse));
        verify(adresseRepository, times(0)).save(any());
    }


    @Test
    public void testUpdateAdress_byValidPersonAndValidAdress_sucess() {

        final Adresse adresse = mock(Adresse.class);
        when(adresse.getId()).thenReturn(26L);
        final Person person = mock(Person.class);
        when(personService.fetchRecordById(any())).thenReturn(Optional.of(person));

        adresseService.updateRecord(1L, adresse);

        verify(adresseRepository, times(1)).save(any());
    }

    @Test
    public void testUpdateAdress_byValidPersonAndInvalidAdress_fail() {

        final Adresse adresse = mock(Adresse.class);
        when(adresse.getId()).thenReturn(null);
        final Person person = mock(Person.class);
        when(personService.fetchRecordById(any())).thenReturn(Optional.of(person));

        Assertions.assertThrows(IllegalArgumentException.class, () -> adresseService.updateRecord(1L, adresse));
        verify(adresseRepository, times(0)).save(any());
    }


    @Test
    public void testUpdateAdress_byInvalidPersonAndValidAdress_fail() {

        final Adresse adresse = mock(Adresse.class);
        when(personService.fetchRecordById(any())).thenReturn(Optional.ofNullable(null));

        Assertions.assertThrows(IllegalArgumentException.class, () -> adresseService.updateRecord(1L, adresse));
        verify(adresseRepository, times(0)).save(any());
    }

    @Override
    protected JpaRepository<Adresse, Long> getMockedRepository() {
        return adresseRepository;
    }

    @Override
    protected AbstractCrudService<Adresse> getService() {
        return adresseService;
    }

    @Override
    protected List<String> getDefaultSortColumn() {
        return Arrays.asList("von", "bis");
    }
}