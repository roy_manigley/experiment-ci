package ch.hftm.se.personenverwaltung.service;

import ch.hftm.se.personenverwaltung.model.Ort;
import ch.hftm.se.personenverwaltung.repository.OrtRepository;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractServiceTest;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractCrudService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class OrtServiceTest extends AbstractServiceTest<Ort> {

    @InjectMocks
    private OrtService ortService;

    @Mock
    private OrtRepository ortRepository;

    public OrtServiceTest() {
        super(Ort.class);
    }

    @Override
    protected JpaRepository<Ort, Long> getMockedRepository() {
        return ortRepository;
    }

    @Override
    protected AbstractCrudService<Ort> getService() {
        return ortService;
    }

    @Override
    protected List<String> getDefaultSortColumn() {
        return Arrays.asList("ort", "plz");
    }

    /*

    @Test
    public void testFetchAllRecords_listOf3_size() {

        final Ort ort_01 = mock(Ort.class);
        final Ort ort_02 = mock(Ort.class);
        final Ort ort_03 = mock(Ort.class);
        when(ortRepository.findAll(ortService.getDefaultSort())).thenReturn(Arrays.asList(ort_01, ort_02, ort_03));

        final List<Ort> records = ortService.fetchAllRecords();
        assertThat(records, hasSize(3));
    }

    @Test
    public void testCreateRecord_ortWithOutId_sucess() {

        final Ort ort_01 = mock(Ort.class);
        when(ort_01.getId()).thenReturn(null);
        when(ortRepository.save(ArgumentMatchers.any())).thenAnswer(mock -> {
            final Ort savedOrt = mock(Ort.class);
            when(savedOrt.getId()).thenReturn(78L);
            return savedOrt;
        });

        final Ort savedOrt = ortService.createRecord(ort_01);
        assertThat(savedOrt.getId(), is(78L));
    }

    @Test
    public void testCreateRecord_ortWithId_fail() {

        final Ort ort_01 = mock(Ort.class);
        when(ort_01.getId()).thenReturn(1L);

        Assertions.assertThrows(IllegalArgumentException.class, () -> ortService.createRecord(ort_01));
    }

    @Test
    public void testUpdateRecord_ortWithOutId_fail() {

        final Ort ort_01 = mock(Ort.class);
        when(ort_01.getId()).thenReturn(null);

        Assertions.assertThrows(IllegalArgumentException.class, () -> ortService.updateRecord(ort_01));
    }

    @Test
    public void testUpdateRecord_ortWithId_success() {

        final Ort ort_01 = mock(Ort.class);
        when(ort_01.getId()).thenReturn(99L);
        when(ortRepository.save(ArgumentMatchers.any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        final Ort savedOrt = ortService.updateRecord(ort_01);

        assertThat(savedOrt.getId(), is(99L));
    }


    @Test
    public void testDeleteRecord_ortWithId_success() {

        final Ort ort_01 = mock(Ort.class);
        when(ort_01.getId()).thenReturn(99L);

        ortService.deleteRecordById(ort_01.getId());

        verify(ortRepository, times(1)).deleteById(ArgumentMatchers.any());
    }

    @Test
    public void testDefaultSort_property_success() {

        final Ort ort_01 = mock(Ort.class);
        when(ort_01.getId()).thenReturn(99L);

        ortService.deleteRecordById(ort_01.getId());
        final Sort.Order defaultSort = ortService.getDefaultSort().getOrderFor("id");

        assertThat(defaultSort, notNullValue());
    }
    */

}