package ch.hftm.se.personenverwaltung.service.abstracts;

import ch.hftm.se.personenverwaltung.model.abstracts.Keyable;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public abstract class AbstractServiceTest<T extends Keyable> {

    private final Class<T> clazz;

    protected AbstractServiceTest(Class<T> clazz) {
        this.clazz = clazz;
    }


    @Test
    public void testFetchAllRecords_listOf3_size() {

        final T record_01 = mock(clazz);
        final T record_02 = mock(clazz);
        final T record_03 = mock(clazz);
        when(getMockedRepository().findAll(getService().getDefaultSort())).thenReturn(Arrays.asList(record_01, record_02, record_03));

        final List<T> records = getService().fetchAllRecords();
        assertThat(records, hasSize(3));

        verify(getMockedRepository(), times(1)).findAll(getService().getDefaultSort());
    }

    @Test
    public void testCreateRecord_recordWithOutId_sucess() {

        final T record_01 = mock(clazz);
        when(record_01.getId()).thenReturn(null);
        when(getMockedRepository().save(any())).thenAnswer(mock -> {
            final T savedRecord = mock(clazz);
            when(savedRecord.getId()).thenReturn(78L);
            return savedRecord;
        });

        final T savedRecord = getService().createRecord(record_01);
        assertThat(savedRecord.getId(), is(78L));
        verify(getMockedRepository(), times(1)).save(any());
    }

    @Test
    public void testCreateRecord_recordWithId_fail() {

        final T record_01 = mock(clazz);
        when(record_01.getId()).thenReturn(1L);

        assertThrows(IllegalArgumentException.class, () -> getService().createRecord(record_01));
        verify(getMockedRepository(), times(0)).save(any());
    }

    @Test
    public void testUpdateRecord_ortWithOutId_fail() {

        final T record_01 = mock(clazz);
        when(record_01.getId()).thenReturn(null);

        assertThrows(IllegalArgumentException.class, () -> getService().updateRecord(record_01));
        verify(getMockedRepository(), times(0)).save(any());
    }

    @Test
    public void testUpdateRecord_recordWithId_success() {

        final T record_01 = mock(clazz);
        when(record_01.getId()).thenReturn(99L);
        when(getMockedRepository().save(any())).thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0));

        final T savedRecord = getService().updateRecord(record_01);

        assertThat(savedRecord.getId(), is(99L));
        verify(getMockedRepository(), times(1)).save(any());
    }


    @Test
    public void testDeleteRecord_recordWithId_success() {

        final T record_01 = mock(clazz);
        when(record_01.getId()).thenReturn(99L);

        getService().deleteRecordById(record_01.getId());

        verify(getMockedRepository(), times(1)).deleteById(any());
    }

    @Test
    public void testDefaultSort_property_success() {

        final T record_01 = mock(clazz);
        when(record_01.getId()).thenReturn(99L);

        getService().deleteRecordById(record_01.getId());
        getDefaultSortColumn().forEach(columnName -> {

            final Sort.Order defaultSort = getService().getDefaultSort().getOrderFor(columnName);
            assertThat(defaultSort, notNullValue());
        });
        verify(getMockedRepository(), times(1)).deleteById(any());

    }

    protected abstract List<String> getDefaultSortColumn();
    protected abstract JpaRepository<T, Long>  getMockedRepository();
    protected abstract AbstractCrudService<T> getService();
}
