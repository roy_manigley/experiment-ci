package ch.hftm.se.personenverwaltung.service;

import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.repository.PersonRepository;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractServiceTest;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractCrudService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Arrays;
import java.util.List;


@ExtendWith(MockitoExtension.class)
public class PersonServiceTest extends AbstractServiceTest<Person> {

    @InjectMocks
    private PersonService personService;

    @Mock
    private PersonRepository personRepository;

    public PersonServiceTest() {
        super(Person.class);
    }

    @Override
    protected JpaRepository<Person, Long> getMockedRepository() {
        return personRepository;
    }

    @Override
    protected AbstractCrudService<Person> getService() {
        return personService;
    }

    @Override
    protected List<String> getDefaultSortColumn() {
        return Arrays.asList("name", "vorname");
    }
}