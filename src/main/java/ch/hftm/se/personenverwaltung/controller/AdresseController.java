package ch.hftm.se.personenverwaltung.controller;

import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.service.AdresseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/api/person/{idPerson}/adressen")
@RestController
public class AdresseController {

    final Logger LOG = LoggerFactory.getLogger(AdresseController.class);

    private final AdresseService service;


    @Autowired
    public AdresseController(AdresseService service) {
        this.service = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Adresse> fetchAllRecords(@PathVariable Long idPerson) {
        return service.findAllByPersonId(idPerson);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<Adresse> fetchRecordById(@PathVariable Long idPerson, @PathVariable Long id) {
        return service.fetchRecordById(id).filter(a -> a.getPersonId().equals(idPerson));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Adresse updateRecord(@PathVariable Long idPerson, @PathVariable Long id, @RequestBody Adresse record) {
        record.setId(id);
        return service.updateRecord(idPerson, record);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Adresse createRecord(@PathVariable Long idPerson, @RequestBody Adresse record) {
        return service.createRecord(idPerson, record);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteRecord(@PathVariable Long idPerson, @PathVariable Long id) {
        service.deleteRecordById(idPerson, id);
    }
}
