package ch.hftm.se.personenverwaltung.controller.abstracts;

import ch.hftm.se.personenverwaltung.model.abstracts.Keyable;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractCrudService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

public class GenericCrudController<T extends Keyable> {

    protected final AbstractCrudService<T> service;

    public GenericCrudController(AbstractCrudService<T> service) {
        this.service = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<T> fetchAllRecords() {
        return service.fetchAllRecords();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<T> fetchRecordById(@PathVariable Long id) {
        return service.fetchRecordById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public T updateRecord(@PathVariable Long id, @RequestBody T record) {
        record.setId(id);
        return service.updateRecord(record);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public T createRecord(@RequestBody T record) {
        return service.createRecord(record);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteRecord(@PathVariable Long id) {
        service.deleteRecordById(id);
    }
}