package ch.hftm.se.personenverwaltung.controller;

import ch.hftm.se.personenverwaltung.controller.abstracts.GenericCrudController;
import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.service.PersonService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RequestMapping("/api/person")
@RestController
public class PersonController extends GenericCrudController<Person> {

    @Autowired
    public PersonController(PersonService service) {
        super(service);
    }

}
