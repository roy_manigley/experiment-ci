package ch.hftm.se.personenverwaltung.controller;

import ch.hftm.se.personenverwaltung.controller.abstracts.GenericCrudController;
import ch.hftm.se.personenverwaltung.model.Ort;
import ch.hftm.se.personenverwaltung.service.OrtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/ort")
@RestController
public class OrtController extends GenericCrudController<Ort> {

    @Autowired
    public OrtController(OrtService service) {
        super(service);
    }
}
