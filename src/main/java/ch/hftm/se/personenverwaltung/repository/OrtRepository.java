package ch.hftm.se.personenverwaltung.repository;

import ch.hftm.se.personenverwaltung.model.Ort;
import ch.hftm.se.personenverwaltung.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrtRepository extends JpaRepository<Ort, Long> {

}
