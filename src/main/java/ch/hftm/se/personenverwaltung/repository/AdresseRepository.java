package ch.hftm.se.personenverwaltung.repository;

import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AdresseRepository extends JpaRepository<Adresse, Long> {

    @Query("FROM Adresse a LEFT JOIN FETCH a.ort WHERE a.person.id = :idPerson")
    public List<Adresse> findAllByIdPerson(Long idPerson);
}
