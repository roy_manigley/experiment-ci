package ch.hftm.se.personenverwaltung.model;

import ch.hftm.se.personenverwaltung.model.abstracts.Keyable;
import ch.hftm.se.personenverwaltung.utils.GueltigeAdresseFilter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "PERSON")
public class Person implements Keyable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "VORNAME")
    private String vorname;

    @Column(name = "GEBURTSDATUM")
    private LocalDate geburtsdatum;

    @JoinColumn(name = "ID_HEIMATORT")
    @ManyToOne(fetch = FetchType.EAGER)
    private Ort heimatort;

    @OneToMany(mappedBy = "person", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<Adresse> adressen = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public LocalDate getGeburtsdatum() {
        return geburtsdatum;
    }

    public void setGeburtsdatum(LocalDate geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    public Ort getHeimatort() {
        return heimatort;
    }

    public void setHeimatort(Ort heimatort) {
        this.heimatort = heimatort;
    }

    public Set<Adresse> getAdressen() {
        return adressen;
    }

    @JsonSerialize
    public Adresse getGueltigeAdresse() {

        return getAdressen().stream()
                .filter(GueltigeAdresseFilter::isAdresseGueltig)
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id) &&
                Objects.equals(name, person.name) &&
                Objects.equals(vorname, person.vorname) &&
                Objects.equals(geburtsdatum, person.geburtsdatum) &&
                Objects.equals(heimatort, person.heimatort);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, vorname, geburtsdatum, heimatort);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", vorname='" + vorname + '\'' +
                ", geburtsdatum=" + geburtsdatum +
                ", heimatort=" + heimatort +
                '}';
    }
}
