package ch.hftm.se.personenverwaltung.model;

import ch.hftm.se.personenverwaltung.model.abstracts.Keyable;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ORT")
public class Ort implements Keyable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "ORT", nullable = false)
    private String ort;

    @Column(name = "PLZ", nullable = false)
    private String plz;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ort ort1 = (Ort) o;
        return Objects.equals(id, ort1.id) &&
                Objects.equals(ort, ort1.ort) &&
                Objects.equals(plz, ort1.plz);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ort, plz);
    }

    @Override
    public String toString() {
        return "Ort{" +
                "id=" + id +
                ", ort='" + ort + '\'' +
                ", plz='" + plz + '\'' +
                '}';
    }
}
