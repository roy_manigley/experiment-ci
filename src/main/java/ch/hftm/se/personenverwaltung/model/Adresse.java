package ch.hftm.se.personenverwaltung.model;

import ch.hftm.se.personenverwaltung.model.abstracts.Keyable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "ADRESSE")
public class Adresse implements Keyable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "STRASSE")
    private String strasse;

    @Column(name = "NUMMER")
    private String nummer;

    @Column(name = "VON")
    private LocalDate von;

    @Column(name = "BIS")
    private LocalDate bis;

    @JoinColumn(name = "ID_ORT")
    @ManyToOne(fetch = FetchType.EAGER)
    private Ort ort;

    @JoinColumn(name = "ID_PERSON")
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Person person;

    @JsonSerialize
    public Long getPersonId() {
        return person != null ? person.getId() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getNummer() {
        return nummer;
    }

    public void setNummer(String nummer) {
        this.nummer = nummer;
    }

    public LocalDate getVon() {
        return von;
    }

    public void setVon(LocalDate von) {
        this.von = von;
    }

    public LocalDate getBis() {
        return bis;
    }

    public void setBis(LocalDate bis) {
        this.bis = bis;
    }

    public Ort getOrt() {
        return ort;
    }

    public void setOrt(Ort ort) {
        this.ort = ort;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Adresse adresse = (Adresse) o;
        return Objects.equals(id, adresse.id) &&
                Objects.equals(strasse, adresse.strasse) &&
                Objects.equals(nummer, adresse.nummer) &&
                Objects.equals(von, adresse.von) &&
                Objects.equals(bis, adresse.bis) &&
                Objects.equals(ort, adresse.ort) &&
                Objects.equals(person, adresse.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, strasse, nummer, von, bis, ort, person);
    }

    @Override
    public String toString() {
        return "Adresse{" +
                "id=" + id +
                ", strasse='" + strasse + '\'' +
                ", nummer='" + nummer + '\'' +
                ", von=" + von +
                ", bis=" + bis +
                ", ort=" + ort +
                ", person=" + person +
                '}';
    }
}
