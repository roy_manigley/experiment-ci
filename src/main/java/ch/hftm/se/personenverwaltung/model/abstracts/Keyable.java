package ch.hftm.se.personenverwaltung.model.abstracts;

public interface Keyable {

    public Long getId();

    public void setId(Long id);
}
