package ch.hftm.se.personenverwaltung.service.abstracts;

import ch.hftm.se.personenverwaltung.model.abstracts.Keyable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public abstract class AbstractCrudService<T extends Keyable> {

    protected final JpaRepository<T, Long> repository;

    public AbstractCrudService(JpaRepository<T, Long> repository) {
        this.repository = repository;
    }

    public List<T> fetchAllRecords() {
        return repository.findAll(getDefaultSort());
    }

    public Optional<T> fetchRecordById(Long id) {
        return repository.findById(id);
    }

    public T createRecord(T record) {
        if (record.getId() != null)
            throw new IllegalArgumentException("Eintrag kann nicht erstellt werden, da bereits eine ID vergeben ist");
        return saveRecord(record);
    }

    public T updateRecord(T record) {
        if (record.getId() == null)
            throw new IllegalArgumentException("Eintrag kann aktualisiert werden, da keine ID vergeben ist");
        return saveRecord(record);
    }

    private T saveRecord(T record) {
        try {
            return repository.save(record);
        } catch (DataIntegrityViolationException e) {
            throw new RuntimeException("Es wurden nicht alle obligatorischen Felder ausgefüllt", e);
        }
    }

    public void deleteRecordById(Long id) {
        try {
            repository.deleteById(id);
        } catch (DataIntegrityViolationException e) {
            throw new RuntimeException("Die Entität kann nicht gelöscht werden, da sie noch von einer anderen Referenziert wird", e);
        } catch (EmptyResultDataAccessException e) {
            throw new IllegalArgumentException("Keine Entität mit dieser Id gefunden (" + id + ")", e);
        }
    }

    public abstract Sort getDefaultSort();
}
