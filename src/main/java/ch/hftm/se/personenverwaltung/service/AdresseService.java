package ch.hftm.se.personenverwaltung.service;

import ch.hftm.se.personenverwaltung.model.Adresse;
import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.repository.AdresseRepository;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class AdresseService extends AbstractCrudService<Adresse> {

    private final PersonService personService;

    @Autowired
    public AdresseService(AdresseRepository repository, PersonService personService) {
        super(repository);
        this.personService = personService;
    }

    public List<Adresse> findAllByPersonId(Long idPerson) {
        getOrElseThrowIfPersonNotPresent(idPerson);
        return ((AdresseRepository) repository).findAllByIdPerson(idPerson);
    }

    public Adresse createRecord(Long idPerson, Adresse record) {
        record.setPerson(
                getOrElseThrowIfPersonNotPresent(idPerson)
        );
        return createRecord(record);
    }

    public Adresse updateRecord(Long idPerson, Adresse record) {
        record.setPerson(
                getOrElseThrowIfPersonNotPresent(idPerson)
        );
        return updateRecord(record);
    }

    private Person getOrElseThrowIfPersonNotPresent(@PathVariable Long idPerson) {
        return personService.fetchRecordById(idPerson).orElseThrow(() -> new IllegalArgumentException("Person nicht gefunden"));
    }

    public void deleteRecordById(Long idPerson, Long id) {
        getOrElseThrowIfPersonNotPresent(idPerson);
        deleteRecordById(id);
    }

    @Override
    public Sort getDefaultSort() {
        return Sort.by(new Sort.Order(Sort.Direction.DESC, "bis", Sort.NullHandling.NULLS_FIRST), Sort.Order.desc("von"));
    }
}
