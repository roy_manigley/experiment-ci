package ch.hftm.se.personenverwaltung.service;

import ch.hftm.se.personenverwaltung.model.Person;
import ch.hftm.se.personenverwaltung.repository.PersonRepository;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class PersonService extends AbstractCrudService<Person> {

    @Autowired
    public PersonService(PersonRepository repository) {
        super(repository);
    }

    @Override
    public Sort getDefaultSort() {
        return Sort.by(Sort.Order.asc("name"), Sort.Order.asc("vorname"));
    }
}
