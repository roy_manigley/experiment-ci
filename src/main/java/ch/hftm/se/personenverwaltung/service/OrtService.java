package ch.hftm.se.personenverwaltung.service;

import ch.hftm.se.personenverwaltung.model.Ort;
import ch.hftm.se.personenverwaltung.repository.OrtRepository;
import ch.hftm.se.personenverwaltung.service.abstracts.AbstractCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class OrtService extends AbstractCrudService<Ort> {

    @Autowired
    public OrtService(OrtRepository repository) {
        super(repository);
    }

    public Sort getDefaultSort() {
        return Sort.by(Sort.Order.asc("ort"), Sort.Order.asc("plz"));
    }
}
