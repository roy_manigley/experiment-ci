package ch.hftm.se.personenverwaltung.utils;

import ch.hftm.se.personenverwaltung.model.Adresse;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class GueltigeAdresseFilter {

    public static Boolean isAdresseGueltig(Adresse adresse) {
        final Period periodBefore = Period.between(adresse.getVon(), LocalDate.now());
        Period periodAfter = null;

        if (adresse.getBis() != null) {
            periodAfter = Period.between(adresse.getBis(), LocalDate.now());
        }

        return periodBefore.get(ChronoUnit.DAYS) + periodBefore.get(ChronoUnit.MONTHS) + periodBefore.get(ChronoUnit.YEARS) >= 0 &&
                (periodAfter == null || periodAfter.get(ChronoUnit.DAYS) + periodAfter.get(ChronoUnit.MONTHS) + periodAfter.get(ChronoUnit.YEARS) < 0);
    }
}
